package com.test.sample;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;
import com.test.sample.utils.MyApplication;
import com.test.sample.utils.ScriptBridge;
import com.test.sample.utils.TimeLoggerEvt;

public class MainActivity extends AppCompatActivity{


    private WebView webView;
    private Context mContext;
    private Tracker mTracker;
    public static String main_url = "http://35.236.146.28";
    long lastPress = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;

        MyApplication application = (MyApplication) getApplication();
        mTracker = application.getDefaultTracker();


        webView = (WebView)findViewById( R.id.webview);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setDomStorageEnabled(true);

        // 로드 시점을 체크하기 위해 webview별로  WebViewClient 를 만들어 주세요
        LocalWebViewClient localWebViewClient = new LocalWebViewClient();
        webView.setWebViewClient(localWebViewClient);

        webView.setWebChromeClient(new WebChromeClient());
        webView.setNetworkAvailable(true);


        webView.getSettings().setDomStorageEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        // 스크립트와 브릿지 하기 위해 JavascriptInterface 추가
        // javascript에서 ScriptBridge 클래스에 있는 함수 호출 가능
        webView.addJavascriptInterface(new ScriptBridge(this,localWebViewClient), "smartAppBridgeForGa");


        webView.loadUrl(main_url);
    }

    public void goBack(View v){
        if (webView.canGoBack()) {
            webView.goBack();
        }
    }

    public void goFoward(View v){
        if (webView.canGoForward()) {
            webView.goForward();
        }
    }

    public void refresh(View v){
        webView.reload();
    }

    public void goHome(View v){
        webView.loadUrl(main_url);
    }


    // 뒤로가기 종료
    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        }else{
            onBackListener();
        }
    }

    public void onBackListener() {
        long currentTime = System.currentTimeMillis();
        if(currentTime - lastPress > 5000){
            Toast.makeText(((Activity) mContext).getBaseContext(), "'뒤로' 버튼을 한번 더 누르시면 종료됩니다.", Toast.LENGTH_LONG).show();
            lastPress = currentTime;
        }else{
            super.onBackPressed();
        }
    }

    // 웹 로드시간 측정을 위한 가이드
    class LocalWebViewClient extends WebViewClient implements TimeLoggerEvt{

        private long webLoadStartTimeStamp = 0;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon){

            // 원하는 시작 시점에 아래 구문을 넣으세요. (추천 시점 = 1번)
            try {
                webLoadStartTimeStamp = System.currentTimeMillis();
                Log.d("시작 = ", Long.toString(webLoadStartTimeStamp));
            }catch (Exception e){}

            // 시작 시점 1번

            super.onPageStarted(view,url,favicon);

            // 시작 시점 2번
        }

        @Override
        public void onPageFinished(WebView view, String url){
            // 시작 시점 3번

            super.onPageFinished(view, url);

            // 시작 시점 4번
        }

        @Override
        public long getStTimeEvt() {
            return webLoadStartTimeStamp;
        }
    }
}
