package com.test.sample.utils;


// 웹 로드시간 측정을 위한 가이드
// WebViewClient 만들때 상속받을 interface
public interface TimeLoggerEvt {

    // web load start time
    public long getStTimeEvt();

}
