package com.test.sample.utils;


import android.os.AsyncTask;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.URLUtil;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.test.sample.MainActivity;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Iterator;


public class ScriptBridge {

    private MyApplication myApp = null;
    private MainActivity activity;

    public final static String GA_LOG_PREFIX = "GA log";
    private boolean LOG_PRINT = false;

    public final static String GA_KEYWORD_DEFAULT_SCREEN_NAME = "mobile app uri is not checked, for qa.";

    // BASE KEWORD
    public final static int CUSTOM_INDEX_LIMIT = 20;
    public final static String GA_PREFIX_DIMENSION = "dimension";
    public final static String GA_PREFIX_METRIC = "metric";

    // PRD KEWORD
    public final static String GA_KEYWORD_PRD_ID = "id";
    public final static String GA_KEYWORD_PRD_NAME = "name";
    public final static String GA_KEYWORD_PRD_BRAND = "brand";
    public final static String GA_KEYWORD_PRD_CATEGORY = "category";
    public final static String GA_KEYWORD_PRD_VARIANT = "variant";
    public final static String GA_KEYWORD_PRD_POSITION = "list_position";
    public final static String GA_KEYWORD_PRD_QUANTITY = "quantity";
    public final static String GA_KEYWORD_PRD_PRICE = "price";
    public final static String GA_KEYWORD_PRD_COUPON = "coupon";

    // PRD ACTION KEYWORD
    public final static String GA_KEYWORD_PRDACTION_TRANSACTION_ID = "transaction_id";
    public final static String GA_KEYWORD_PRDACTION_AFFILIATION = "affiliation";
    public final static String GA_KEYWORD_PRDACTION_VALUE = "value";
    public final static String GA_KEYWORD_PRDACTION_CURRENCY = "currency";
    public final static String GA_KEYWORD_PRDACTION_TAX = "tax";
    public final static String GA_KEYWORD_PRDACTION_SHIPPING = "shipping";
    public final static String GA_KEYWORD_PRDACTION_CHECKOUT_STEP = "checkout_step";
    public final static String GA_KEYWORD_PRDACTION_CHECKOUT_OPTION = "checkout_option";
    public final static String GA_KEYWORD_PRDACTION_ITEMS = "items";

    // EVENT KEYWORD
    public final static String GA_KEYWORD_EVENT_CATEGORY = "category";
    public final static String GA_KEYWORD_EVENT_ACTION = "action";
    public final static String GA_KEYWORD_EVENT_LABEL = "label";
    public final static String GA_KEYWORD_EVENT_VALUE = "value";

    // TIMING KEYWORD
    public final static String GA_KEYWORD_TIMING_CATEGORY = "event_category";
    public final static String GA_KEYWORD_TIMING_VALUE = "value";
    public final static String GA_KEYWORD_TIMING_NAME= "name";
    public final static String GA_KEYWORD_TIMING_LABEL = "event_label";



    private Tracker mTracker;
    private TimeLoggerEvt evtLogger;


    public ScriptBridge(MainActivity mainActivity,TimeLoggerEvt evtlogger) {
        try{
            activity = mainActivity;
            // ga tracker를 설정했던 Application 가져오기
            myApp = (MyApplication) activity.getApplication();
            mTracker = myApp.getDefaultTracker();
            evtLogger = evtlogger;
        }catch(Exception e){
            if(LOG_PRINT) Log.d(GA_LOG_PREFIX,"interface init error :" + e.toString());
        }
    }


    // uid 설정
    @JavascriptInterface
    public void setId(final String userId){
        asyncRun(new AsyncRunner() {
            @Override
            public void run() {

                try{
                    synchronized (mTracker){
                        if (!isStrEmpty(userId)){
                            mTracker.set("&uid", userId);
                        }
                    }
                } catch (Exception e){
                    if(LOG_PRINT) Log.d(GA_LOG_PREFIX,"setId error :" + e.toString());
                }

            }
        });
    }

    // currency 설정
    @JavascriptInterface
    public void setCurrency(final String currency){
        asyncRun(new AsyncRunner() {
            @Override
            public void run() {
                synchronized (mTracker){
                    try{
                        if (!isStrEmpty(currency)){
                            mTracker.set("&cu", currency);
                        }
                    }catch (Exception e){
                        if(LOG_PRINT) Log.d(GA_LOG_PREFIX,"setCurrency error :" + e.toString());
                    }
                }
            }
        });
    }


    //전자상거래 발송
    @JavascriptInterface
    public void sendEc(final String viewName, final String prdActionName, final String data){

        asyncRun(new AsyncRunner() {
            @Override
            public void run() {
                HitBuilders.ScreenViewBuilder hit = new HitBuilders.ScreenViewBuilder();

                try{
                    String prdActName = getPrdActionName(prdActionName);

                    if (!isStrEmpty(prdActName) && !isStrEmpty(data)){
                        ProductAction prdAction = new ProductAction(prdActName);

                        String dataKind = checkJsonData(data);
                        if("object".equals(dataKind)){
                            JSONObject obj = new JSONObject(data);
                            Iterator<String> tmp = obj.keys();

                            while (tmp.hasNext()) {
                                String key = tmp.next();
                                Object val = obj.get(key);

                                if(GA_KEYWORD_PRDACTION_TRANSACTION_ID.equals(key)){
                                    prdAction.setTransactionId(val.toString());
                                } else if(GA_KEYWORD_PRDACTION_AFFILIATION.equals(key)){
                                    prdAction.setTransactionAffiliation(val.toString());
                                } else if(GA_KEYWORD_PRDACTION_VALUE.equals(key)){
                                    if(val instanceof Integer){
                                        double dnum = Double.valueOf(val.toString());
                                        prdAction.setTransactionRevenue(dnum);
                                    } else if(val instanceof Double){
                                        prdAction.setTransactionRevenue((Double) val);
                                    }
                                }else if(GA_KEYWORD_PRDACTION_TAX.equals(key)){
                                    if(val instanceof Integer){
                                        double dnum = Double.valueOf(val.toString());
                                        prdAction.setTransactionTax(dnum);
                                    } else if(val instanceof Double){
                                        prdAction.setTransactionTax((Double) val);
                                    }
                                } else if(GA_KEYWORD_PRDACTION_SHIPPING.equals(key)){
                                    if(val instanceof Integer){
                                        double dnum = Double.valueOf(val.toString());
                                        prdAction.setTransactionShipping(dnum);
                                    } else if(val instanceof Double){
                                        prdAction.setTransactionShipping((Double) val);
                                    }
                                } else if(GA_KEYWORD_PRDACTION_CHECKOUT_STEP.equals(key)){
                                    if(val instanceof Integer){
                                        prdAction.setCheckoutStep((Integer) val);
                                    }
                                } else if(GA_KEYWORD_PRDACTION_CHECKOUT_OPTION.equals(key)){
                                    prdAction.setCheckoutOptions(val.toString());
                                } else if(GA_KEYWORD_PRDACTION_ITEMS.equals(key)){
                                    String isJson = checkJsonData(val.toString());
                                    if(isJson.equals("array")){
                                        JSONArray array = new JSONArray(val.toString());

                                        for (int i = 0; i < array.length(); i++) {
                                            JSONObject item = array.getJSONObject(i);
                                            Product prd = makeProduct(item);
                                            if(prd != null){
                                                hit.addProduct(prd);
                                            }
                                        }
                                    }
                                } else if(key.startsWith(GA_PREFIX_DIMENSION)){
                                    int index = getIndexFromKey(key, GA_PREFIX_DIMENSION);
                                    if(index != 0){
                                        hit.setCustomDimension(index, val.toString());
                                    }
                                } else if(key.startsWith(GA_PREFIX_METRIC)){
                                    int index = getIndexFromKey(key, GA_PREFIX_METRIC);
                                    if(index != 0){
                                        if(val instanceof Integer || val instanceof Double) {
                                            float fnum = Float.valueOf(val.toString());
                                            hit.setCustomMetric(index, fnum);
                                        }
                                    }
                                }
                            }
                        } else {
                            return;
                        }

                        hit.setProductAction(prdAction);

                        synchronized (mTracker) {
                            if (!isStrEmpty(viewName)) {
                                mTracker.setScreenName(viewName);
                                if (URLUtil.isValidUrl(viewName)) {
                                    hit.setCampaignParamsFromUrl(viewName);
                                }
                            } else {
                                mTracker.setScreenName(GA_KEYWORD_DEFAULT_SCREEN_NAME);
                            }

                            mTracker.send(hit.build());
                        }
                    }

                } catch(Exception e){
                    if(LOG_PRINT) Log.d(GA_LOG_PREFIX,"sendEc error :" + e.toString());
                }
            }
        });
    }


    // 이벤트 발송 (category, action 필수)
    @JavascriptInterface
    public void sendEvent(final String viewName, final String data){
        asyncRun(new AsyncRunner() {
            @Override
            public void run() {
                HitBuilders.EventBuilder hit = new HitBuilders.EventBuilder();

                try{
                    if (!isStrEmpty(data)){

                        String dataKind = checkJsonData(data);
                        if("object".equals(dataKind)){
                            JSONObject obj = new JSONObject(data);
                            Iterator<String> tmp = obj.keys();

                            // category, action은 필수값
                            if(obj.isNull(GA_KEYWORD_EVENT_CATEGORY) || obj.isNull(GA_KEYWORD_EVENT_ACTION)){
                                return;
                            }

                            while (tmp.hasNext()) {
                                String key = tmp.next();
                                Object val = obj.get(key);

                                if(GA_KEYWORD_EVENT_CATEGORY.equals(key)){
                                    hit.setCategory(val.toString());
                                } else if(GA_KEYWORD_EVENT_ACTION.equals(key)){
                                    hit.setAction(val.toString());
                                } else if(GA_KEYWORD_EVENT_LABEL.equals(key)){
                                    hit.setLabel(val.toString());
                                } else if(GA_KEYWORD_EVENT_VALUE.equals(key)){
                                    if(val instanceof Integer){
                                        long lnum = Long.valueOf(val.toString());
                                        hit.setValue(lnum);
                                    }
                                } else if(key.startsWith(GA_PREFIX_DIMENSION)){
                                    int index = getIndexFromKey(key, GA_PREFIX_DIMENSION);
                                    if(index != 0){
                                        hit.setCustomDimension(index, val.toString());
                                    }
                                } else if(key.startsWith(GA_PREFIX_METRIC)){
                                    int index = getIndexFromKey(key, GA_PREFIX_METRIC);
                                    if(index != 0){
                                        if(val instanceof Integer || val instanceof Double) {
                                            float fnum = Float.valueOf(val.toString());
                                            hit.setCustomMetric(index, fnum);
                                        }
                                    }
                                }
                            }

                            synchronized (mTracker){
                                if (!isStrEmpty(viewName)){
                                    mTracker.setScreenName(viewName);
                                    if (URLUtil.isValidUrl(viewName)) {
                                        hit.setCampaignParamsFromUrl(viewName);
                                    }
                                } else {
                                    mTracker.setScreenName(GA_KEYWORD_DEFAULT_SCREEN_NAME);
                                }

                                mTracker.send(hit.build());
                            }

                        }
                    }

                }catch(Exception e){
                    if(LOG_PRINT) Log.d(GA_LOG_PREFIX,"sendEvent error :" + e.toString());
                }
            }
        });
    }


    // 스크린뷰 (페이지뷰) 발송
    @JavascriptInterface
    public void sendView(final String viewName, final String data){
        asyncRun(new AsyncRunner() {
            @Override
            public void run() {
                HitBuilders.ScreenViewBuilder hit = new HitBuilders.ScreenViewBuilder();

                try{
                    if (!isStrEmpty(data)){
                        JSONObject obj = new JSONObject(data);
                        Iterator<String> tmp = obj.keys();

                        while (tmp.hasNext()) {
                            String key = tmp.next();
                            Object val = obj.get(key);

                            if(key.startsWith(GA_PREFIX_DIMENSION)){
                                int index = getIndexFromKey(key, GA_PREFIX_DIMENSION);
                                if(index != 0){
                                    hit.setCustomDimension(index, val.toString());
                                }
                            } else if(key.startsWith(GA_PREFIX_METRIC)){
                                int index = getIndexFromKey(key, GA_PREFIX_METRIC);
                                if(index != 0){
                                    if(val instanceof Integer || val instanceof Double){
                                        float fnum = Float.valueOf(val.toString());
                                        hit.setCustomMetric(index, fnum);
                                    }
                                }
                            }
                        }

                        synchronized (mTracker){
                            if (!isStrEmpty(viewName)){
                                mTracker.setScreenName(viewName);
                                if (URLUtil.isValidUrl(viewName)) {
                                    hit.setCampaignParamsFromUrl(viewName);
                                }
                            } else {
                                mTracker.setScreenName(GA_KEYWORD_DEFAULT_SCREEN_NAME);
                            }
                            mTracker.send(hit.build());
                        }

                    } else {
                        sendView(viewName);
                    }
                }catch(Exception e){
                    if(LOG_PRINT) Log.d(GA_LOG_PREFIX,"sendView error :" + e.toString());
                }
            }
        });
    }

    // 데이터 없이 스크린뷰 (페이지뷰) 발송
    @JavascriptInterface
    public void sendView(final String viewName){
        asyncRun(new AsyncRunner() {
            @Override
            public void run() {
                HitBuilders.ScreenViewBuilder hit = new HitBuilders.ScreenViewBuilder();
                try{
                    synchronized (mTracker) {
                        if (!isStrEmpty(viewName)) {
                            mTracker.setScreenName(viewName);
                            if (URLUtil.isValidUrl(viewName)) {
                                hit.setCampaignParamsFromUrl(viewName);
                            }
                        } else {
                            mTracker.setScreenName(GA_KEYWORD_DEFAULT_SCREEN_NAME);
                        }
                        mTracker.send(hit.build());
                    }

                }catch (Exception e){
                    if(LOG_PRINT) Log.d(GA_LOG_PREFIX,"sendView_1 error :" + e.toString());
                }
            }
        });
    }


    // 타이밍 발송
    // event_category, value, name 필수
    @JavascriptInterface
    public void sendTiming(final String viewName, final String data){
        asyncRun(new AsyncRunner() {
            @Override
            public void run() {
                HitBuilders.TimingBuilder hit = new HitBuilders.TimingBuilder();

                try{
                    if (!isStrEmpty(data)){

                        String dataKind = checkJsonData(data);
                        if("object".equals(dataKind)){
                            JSONObject obj = new JSONObject(data);
                            Iterator<String> tmp = obj.keys();

                            if(obj.isNull(GA_KEYWORD_TIMING_CATEGORY) || obj.isNull(GA_KEYWORD_TIMING_VALUE) || obj.isNull(GA_KEYWORD_TIMING_NAME)){
                                return;
                            }

                            while (tmp.hasNext()) {
                                String key = tmp.next();
                                Object val = obj.get(key);

                                if(GA_KEYWORD_TIMING_CATEGORY.equals(key)){
                                    hit.setCategory(val.toString());
                                } else if(GA_KEYWORD_TIMING_VALUE.equals(key)){
                                    if(val instanceof Integer || val instanceof Long){
                                        long lnum = Long.valueOf(val.toString());
                                        hit.setValue(lnum);
                                    } else {
                                        return;
                                    }
                                } else if(GA_KEYWORD_TIMING_NAME.equals(key)){
                                    hit.setVariable(val.toString());
                                } else if(GA_KEYWORD_TIMING_LABEL.equals(key)){
                                    hit.setLabel(val.toString());
                                } else if(key.startsWith(GA_PREFIX_DIMENSION)){
                                    int index = getIndexFromKey(key, GA_PREFIX_DIMENSION);
                                    if(index != 0){
                                        hit.setCustomDimension(index, val.toString());
                                    }
                                } else if(key.startsWith(GA_PREFIX_METRIC)){
                                    int index = getIndexFromKey(key, GA_PREFIX_METRIC);
                                    if(index != 0){
                                        if(val instanceof Integer || val instanceof Double) {
                                            float fnum = Float.valueOf(val.toString());
                                            hit.setCustomMetric(index, fnum);
                                        }
                                    }
                                }
                            }

                            synchronized (mTracker){
                                if (!isStrEmpty(viewName)){
                                    mTracker.setScreenName(viewName);
                                    if (URLUtil.isValidUrl(viewName)) {
                                        hit.setCampaignParamsFromUrl(viewName);
                                    }
                                } else {
                                    mTracker.setScreenName(GA_KEYWORD_DEFAULT_SCREEN_NAME);
                                }

                                mTracker.send(hit.build());
                            }

                        }
                    }

                }catch(Exception e){
                    if(LOG_PRINT) Log.d(GA_LOG_PREFIX,"sendTiming error :" + e.toString());
                }
            }
        });
    }


    // 페이지 로드 길이 잡기
    // 타이밍 계산하고 싶은 시점에 호출 (event_category, name 필수)
    @JavascriptInterface
    public void sendLoadPeriod(final String viewName, final String data){
        asyncRun(new AsyncRunner() {
            @Override
            public void run() {
                HitBuilders.TimingBuilder hit = new HitBuilders.TimingBuilder();

                try{
                    if (!isStrEmpty(data)){

                        long startTimeStamp = evtLogger.getStTimeEvt();
                        long currentTimeStamp = System.currentTimeMillis();

                        if(startTimeStamp != 0){
                            long value = currentTimeStamp - startTimeStamp;

                            if(value > 0){
                                hit.setValue(value);
                            } else {
                                return;
                            }
                        } else{
                            return;
                        }

                        String dataKind = checkJsonData(data);
                        if("object".equals(dataKind)){
                            JSONObject obj = new JSONObject(data);
                            Iterator<String> tmp = obj.keys();

                            if(obj.isNull(GA_KEYWORD_TIMING_CATEGORY) || obj.isNull(GA_KEYWORD_TIMING_NAME)){
                                return;
                            }

                            while (tmp.hasNext()) {
                                String key = tmp.next();
                                Object val = obj.get(key);

                                if(GA_KEYWORD_TIMING_CATEGORY.equals(key)){
                                    hit.setCategory(val.toString());
                                } else if(GA_KEYWORD_TIMING_NAME.equals(key)){
                                    hit.setVariable(val.toString());
                                } else if(GA_KEYWORD_TIMING_LABEL.equals(key)){
                                    hit.setLabel(val.toString());
                                } else if(key.startsWith(GA_PREFIX_DIMENSION)){
                                    int index = getIndexFromKey(key, GA_PREFIX_DIMENSION);
                                    if(index != 0){
                                        hit.setCustomDimension(index, val.toString());
                                    }
                                } else if(key.startsWith(GA_PREFIX_METRIC)){
                                    int index = getIndexFromKey(key, GA_PREFIX_METRIC);
                                    if(index != 0){
                                        if(val instanceof Integer || val instanceof Double) {
                                            float fnum = Float.valueOf(val.toString());
                                            hit.setCustomMetric(index, fnum);
                                        }
                                    }
                                }
                            }

                            synchronized (mTracker){
                                if (!isStrEmpty(viewName)){
                                    mTracker.setScreenName(viewName);
                                } else {
                                    mTracker.setScreenName(GA_KEYWORD_DEFAULT_SCREEN_NAME);
                                }

                                mTracker.send(hit.build());
                            }

                        }
                    }

                }catch(Exception e){
                    if(LOG_PRINT) Log.d(GA_LOG_PREFIX,"sendLoadPeriod error :" + e.toString());
                }
            }
        });
    }



    // 전자상거래 발송에 쓰일 액션
    private String getPrdActionName(String name){
        String result = "";
        try{
            if(!isStrEmpty(name)){
                if("select_content".equals(name)){
                    result = ProductAction.ACTION_CLICK;
                } else if("view_item".equals(name)){
                    result = ProductAction.ACTION_DETAIL;
                } else if("add_to_cart".equals(name)){
                    result = ProductAction.ACTION_ADD;
                } else if("remove_from_cart".equals(name)){
                    result = ProductAction.ACTION_REMOVE;
                } else if("begin_checkout".equals(name)){
                    result = ProductAction.ACTION_CHECKOUT;
                } else if("set_checkout_option".equals(name)){
                    result = ProductAction.ACTION_CHECKOUT_OPTION;
                } else if("purchase".equals(name)){
                    result = ProductAction.ACTION_PURCHASE;
                } else if("refund".equals(name)){
                    result = ProductAction.ACTION_REFUND;
                } else{
                    result = "";
                }
            }

        } catch (Exception e){
            result = "";
        }
        return result;
    }

    // 전자성거래 발송에 쓰일 상품
    private Product makeProduct(JSONObject obj){
        Product prd = new Product();

        try{
            if (obj != null){
                Iterator<String> tmp = obj.keys();

                while (tmp.hasNext()) {
                    String key = tmp.next();
                    Object val = obj.get(key);

                    if(GA_KEYWORD_PRD_ID.equals(key)){
                        prd.setId(val.toString());
                    } else if(GA_KEYWORD_PRD_NAME.equals(key)){
                        prd.setName(val.toString());
                    } else if(GA_KEYWORD_PRD_CATEGORY.equals(key)){
                        prd.setCategory(val.toString());
                    } else if(GA_KEYWORD_PRD_BRAND.equals(key)){
                        prd.setBrand(val.toString());
                    } else if(GA_KEYWORD_PRD_VARIANT.equals(key)){
                        prd.setVariant(val.toString());
                    } else if(GA_KEYWORD_PRD_POSITION.equals(key)){
                        if(val instanceof Integer){
                            prd.setPosition((Integer) val);
                        }
                    } else if(GA_KEYWORD_PRD_PRICE.equals(key)){
                        if(val instanceof Integer){
                            double dnum = Double.valueOf(val.toString());
                            prd.setPrice(dnum);
                        } else if(val instanceof Double){
                            prd.setPrice((Double) val);
                        }
                    } else if(GA_KEYWORD_PRD_QUANTITY.equals(key)){
                        if(val instanceof Integer){
                            prd.setQuantity((Integer) val);
                        }
                    } else if(GA_KEYWORD_PRD_COUPON.equals(key)){
                        prd.setCouponCode(val.toString());
                    } else if(key.startsWith(GA_PREFIX_DIMENSION)){
                        int index = getIndexFromKey(key, GA_PREFIX_DIMENSION);
                        if(index != 0){
                            prd.setCustomDimension(index, val.toString());
                        }
                    } else if(key.startsWith(GA_PREFIX_METRIC)){
                        int index = getIndexFromKey(key, GA_PREFIX_METRIC);
                        if(index != 0){
                            if(val instanceof Integer){
                                prd.setCustomMetric(index, (Integer)val);
                            }
                        }
                    }
                }
            } else {
                return null;
            }
        }catch (Exception e){
            return null;
        }
        return prd;
    }

    private boolean strIsDouble(String value){
        boolean result = false;
        try{
            Double.parseDouble(value);
            result = true;
        }catch (Exception e){
            result = false;
        }
        return result;
    }

    private boolean strIsLong(String value){
        boolean result = false;
        try{
            Long.parseLong(value);
            result = true;
        }catch (Exception e){
            result = false;
        }
        return result;
    }

    private boolean isStrEmpty(String str){
        boolean result = true;
        try{
            String tmp = str.trim();
            if (tmp != null && !tmp.isEmpty() && !tmp.equals("null")){
                result = false;
            }
        }catch (Exception e){
            result = true;
        }
        return result;
    }

    private String checkJsonData(String data){
        String result = "string";
        try{
            Object json = new JSONTokener(data).nextValue();
            if (json instanceof JSONObject) {
                result = "object";
            }else if (json instanceof JSONArray){
                result = "array";
            } else{
                result = "string";
            }
        }catch (Exception e){}

        return result;
    }

    // dimension or metric의 key 가져오기
    private int getIndexFromKey(String key, String prefix){
        int result = 0;
        String tmp_key = key.replace(prefix, "");

        try{
            result = Integer.parseInt(tmp_key);
            if(result <= 0 || result > CUSTOM_INDEX_LIMIT){
                result = 0;
            }
        }catch (Exception e){
            return 0;
        }
        return result;
    }


    public void asyncRun(final AsyncRunner asyncRunner){
        AsyncTask<Void,Void,Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        asyncRunner.run();
                    }
                });
                return null;
            }
        };
        task.execute();
    }

    interface AsyncRunner{
        public void run();
    }
}
