package com.test.sample.utils;

import android.app.Application;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.test.sample.R;

/*
    기존에 사용중인 Application이 있다면
    해당부분 내용들만 복사하여 추가
*/

public class MyApplication extends Application {

    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    @Override
    public void onCreate() {
        super.onCreate();

        // tracker 사용을 위해 GoogleAnalytics 생성
        try{
            sAnalytics = GoogleAnalytics.getInstance(this);

            // 디스패치 길이
            sAnalytics.setLocalDispatchPeriod(30);

            // 디버깅 모드
            sAnalytics.setDryRun(false);

        }catch (Exception e){

        }
    }

    //tracker 사용시 해당 함수를 호출하여 사용
    synchronized public Tracker getDefaultTracker() {
        try{
            if (sTracker == null) {
                sTracker = sAnalytics.newTracker(R.xml.ga_tracker);
                sTracker.enableAdvertisingIdCollection(true);
            }

        } catch (Exception e){
            Log.d("GA = ","Google Analytics init fail");
            return null;
        }
        return sTracker;
    }

}
